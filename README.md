# Workshop
- [Link to the **slides**](https://docs.google.com/presentation/d/1eZWpfYbe73XirvlBW20UIPfPKOJxhyOhg75USfq5tQM/edit?usp=sharing)
- [Link to the **projects**](https://gitlab.com/workshops-mcsnyk)
- [Link to the Snyk **organisation**](https://app.snyk.io/org/sb-5hc/projects)

## 1. Warm up excercises
### IDE:

To start with, we will take a look at how Snyk works in the IDEs and in the CLI on your machines. 
We will use a Javascript-Typescript project called "[Juice Shop](https://github.com/alexeisnyk/juice-shop)"

If you wish to try it out on your computer, you can find the project [here](https://github.com/alexeisnyk/juice-shop) - please clone it to your machine! 

_If you use a Jetbrains IDE you can install Snyk here: Preferences > Plugins > Marketplace > Snyk_

- [ ] Run Snyk, observe the categories "Open Source", "Code Security", "Code Quality", "Configuration", "Container"

### CLI:
Snyk can be run in the CLI, as well: the results are shown in an aggregated form.

**Snyk needs to be [installed](https://docs.snyk.io/snyk-cli/install-the-snyk-cli) first**. 

- install with npm: `npm install snyk -g`  or `npm install --location=global snyk`  
- install with yarn: `yarn global add snyk`  
   
In order to create html-reports, there is an SDK called [snyk-to-html](https://github.com/snyk/snyk-to-html)

- install with npm: `npm install snyk-to-html -g`  or `npm install --location=global snyk-to-html`  


Various CLI commands can be found [here](https://docs.snyk.io/snyk-cli/cli-reference), by using `--help` Snyk tells us what extensions exist. 

**Test: ** check projects for vulnerabilities (issues listed up, they can be exported)    
- [ ] Run the following command in order to detect vulnerabilities in the IaC configuration: `snyk iac test`
- [ ] Export the results into a html-file: snyk iac test --json | snyk-to-html>snyk_iac_results.html

**Monitor: ** to observe/track, how the vulnerabilities evolve over time: snapshot is sent to the Snyk WebUI (issues not listed):
- [ ] We set the target organisation by configuring an env. variable: `export SNYK_TOKEN=c60a62ae-0f0e-44c2-b51f-761c7e7ef8dc`  
- [ ] Run the following command in order to monitor Open Source vulnerabilities: `snyk monitor --all-projects`


## 2. Moving towards the Snyk WebUI

![](workshop.png)

## 2.1 Importing Java projects from Gitlab: SCM (native) and CLI integrations 
In case of Javascript, Typescript, Python projects there is no difference between the SCM and the CLI integration.
We should be very careful with Java, Scala, C# or C++ projects on the other side: these projects need to be built in order to be able to draw the dependency graph. Without building these, the scan results in a very inaccurate scan, lots of vulns cannot be detected.

Let's try it out!   

- [ ] First, let's import the **[gradle-goof](https://gitlab.com/workshops-mcsnyk/gradle-goof)** application natively (Gitlab integration pre-configured)  
> How can we open a fixPR to fix vulnerabilities for Gradle projects?
> What do you suggest, isn't this application that bad so that we see a few vulnerabilities?   

- [ ] Let's run the Gitlab CI [pipeline](https://gitlab.com/workshops-mcsnyk/gradle-goof/-/blob/master/.gitlab-ci.yml), build the application and see the results in the Snyk WebUI!
- [ ] Integrate Snyk-monitor into the pipeline, find the reports among the artifacts: it is possible to get a reporting this way, too.
> Ok, that's nice, but is there a way to open a fixPR to get rid of these issues?

- [ ] Let's import a Maven project then, it's called **[java-goof](https://gitlab.com/workshops-mcsnyk/java-goof)**
> Conclusion: pom.xml files are … xml files, so they’re easy to traverse and update. However, Gradle files are similar to code files and are a lot more difficult to update, which is why Snyk doesn’t support them for fix PRs (yet).   
> [Snyk CLI with Gradle](https://docs.snyk.io/products/snyk-open-source/language-and-package-manager-support/snyk-for-java-gradle-maven#gradle): To build the dependency graph, Snyk integrates with Gradle and inspects the dependencies returned by the build. The following manifest files are supported: build.gradle and build.gradle.kts
- [ ] open a fixPR, fix an issue!   

## 2.2 Import from a K8s cluster, additional Snyk tests
We've imported the **[java-goof](https://gitlab.com/workshops-mcsnyk/java-goof)** application.    
The application is built, pushed to AWS ECR (Elastic Container Registry). On AWS I set up a K8s cluster, where we'd like to deploy the built application.   

You can find a javagoof-deploy.yaml Helm-chart deployment file.
- [ ] before moving to AWS, take a look at the file first!
>  What security issues do you notice at the first glance?

- [ ] Now analyse the results in the Snyk WebUI!
> What is the benefit from knowing these issues?

